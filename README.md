esp32-wifi-manager
------------------

WiFi Manager for ESP32. That's it. No captive portal, no fallback if AP is not available. Just WiFi Management.

# Why
Most ESP32 WiFI Manager solutions are just hotspot captive portals that are used to (maybe) scan for and then
connect to an Access Point in the area. This is cumbersome if you want your device to be portable, connecting
to know APs in the area.

This is intended to do one job. Handle connecting to WiFi. Feeding in the AP config info is up to you. But I
trust you and have faith you can do it!

# What ~~*does*~~*will* it do?

* Stores info for **multiple** APs
* Connects to "best"\* AP available
* Remembers which AP was connected to last
* Support more than basic auth
* Tells you you're a pretty alrigh person

\* **best** to be determined

# How do I feed this thing?
Only basic `ssid` and `password` authenication are supported at the moment but adding will be the same in the future.
The API method wifi_manager_add_ap is used to add an Access Point to the config. It is automatically saved and if it
is current not connected, it will attempt to make a connection.

# API

## `EventGroupHandle_t wifi_manager_start()`
Creates EventGroup and starts main WiFi Manager task loop.

## `uint8_t wifi_manager_ap_count()`
Return number of AP in storage.

## `uint8_t wifi_manager_add_ap(char *ssid, char *password)`
Adds AP to storage. !This will change! Will become a struct config.

## `uint8_t wifi_manager_bootstrap_config()`
Loads build time sdkconfig variables into WiFi Manager.

# `EventGroupHandle_t wm_event_group Bits`

## `WIFI_CONNECTED`
WiFi connected has been successful.

## `WIFI_CONNECTING`
WiFi Manager is attempting to make a connection.

## `AP_AVAILABLE`		
AP info has been retreived from storage.
This is in flux, the naming is potentially confusing. 
