#ifndef __WIFI_MANAGER_H
#define __WIFI_MANAGER_H

#define WIFI_MANAGER_MAGIC (0xb00b000c)

void wifi_manager_save_config();
void wifi_manager_load_config();
void wifi_manager_reset_store();

#define WIFI_CONNECTED		(BIT0)
#define WIFI_CONNECTING		(BIT1)
#define AP_AVAILABLE		  (BIT2)
#define WIFI_IDLE   		  (BIT3)
#define WIFI_SCANNING   	(BIT4)

uint8_t wifi_manager_ap_count();
uint8_t wifi_manager_add_ap(char *essid, char *password);

void wifi_manager_connect();
EventGroupHandle_t wifi_manager_start();

int wifi_manager_bootstrap_config();

#endif
