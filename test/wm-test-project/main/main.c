#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "driver/uart.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "sdkconfig.h"

#include "esp32-wifi-manager.h"

#define TAG "WM-TEST"
#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
#define EX_UART_NUM UART_NUM_0

#define ECHO_UART0  (true)

static uint8_t id;
static QueueHandle_t uart0_queue;

EventGroupHandle_t wm_event_group;

static void uart_event_task(void *pvParameters) {
  uart_event_t event;
  uint8_t *dtmp = (uint8_t*) malloc(RD_BUF_SIZE);
  uint8_t size = 0;

  bzero(dtmp, RD_BUF_SIZE);

  for(;;) {
    if(xQueueReceive(uart0_queue, (void * )&event, (portTickType)portMAX_DELAY)) {
      switch(event.type) {
        case UART_DATA:
          uart_read_bytes(UART_NUM_0, dtmp + size, event.size, portMAX_DELAY);
#ifdef ECHO_UART0          
          uart_write_bytes(UART_NUM_0, (const char*) dtmp + size, event.size);
#endif

          size += event.size;

          if(dtmp[size - 1] == '\n') {
            uart_write_bytes(UART_NUM_0, (const char*) dtmp, size);

            bzero(dtmp, RD_BUF_SIZE);
            size = 0;
          }

          break;

          case UART_BREAK:
          case UART_BUFFER_FULL:
          case UART_FIFO_OVF:
          case UART_FRAME_ERR:
          case UART_PARITY_ERR:
          case UART_DATA_BREAK:
          case UART_PATTERN_DET:
          case UART_EVENT_MAX:
          break;
      }
    }
  }
}

void loop(void *p) {
  ESP_LOGI(TAG, "starting main loop");

  while(true) {
    EventBits_t ev_bits = xEventGroupGetBits(wm_event_group);

    if(ev_bits & WIFI_CONNECTED) {
      ESP_LOGI(TAG, "Got connection!");
    } else if(ev_bits & WIFI_IDLE) {
      ESP_LOGI(TAG, "WiFi idle.");

      vTaskDelay(10000 / portTICK_PERIOD_MS);

      ESP_LOGI(TAG, "Attempting reconnect");

      wifi_manager_connect();
    }

    vTaskDelay(10000 / portTICK_PERIOD_MS);
  }
}

void app_main() {
  esp_err_t ret;

  // Initialize NVS.
  ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
    ESP_LOGI(TAG, "Erasing flash memory");
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK( ret );

  uart_config_t uart_config = {
    .baud_rate = 115200,
    .data_bits = UART_DATA_8_BITS,
    .parity = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
  };

  esp_log_level_set(TAG, ESP_LOG_INFO);
  ESP_ERROR_CHECK(uart_param_config(EX_UART_NUM, &uart_config));
  ESP_ERROR_CHECK(uart_set_pin(EX_UART_NUM, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE));
  ESP_ERROR_CHECK(uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart0_queue, 0));

  //Create a task to handler UART event from ISR
  xTaskCreate(uart_event_task, "uart_event_task", 2048, NULL, 1, NULL);

  uint8_t *mac;
  mac = (uint8_t *)malloc(6);
  esp_efuse_mac_get_default(mac);
  id = mac[5];

  ESP_LOGI(TAG, "MAC: %X:%X:%X:%X:%X:%X\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  wm_event_group = wifi_manager_start();
  wifi_manager_reset_store();

  if(wifi_manager_ap_count() == 0) {
    wifi_manager_bootstrap_config();
  } else {
    ESP_LOGI(TAG, "Got AP from store, waiting for connection");
  }

  xTaskCreate(&loop, "loop",  2048, NULL, 6, NULL);
};
